Flat Design Character Maker
===================

Just a simple flat design vector based character designer.

![](https://raw.githubusercontent.com/michaelsboost/Flat-Design-Character-Maker/gh-pages/screenshot.png)

Version
-------------

0.0.1

License
-------------

MIT

Availability
-------------

[Android](https://play.google.com/store/apps/details?id=com.webdgap.flatdesigncharactermaker)

If you want this as a desktop application you can use
[WebDGap](http://michaelsboost.github.io/WebDGap)  

Tech
-------------

Flat Design Character Maker uses a number of open source projects to work properly:

* [JQWidgets](http://www.jqwidgets.com/jquery-widgets-demo/demos/jqxsplitter/index.htm#demos/jqxsplitter/nested-splitters.htm) - Resizable columns
* [JQuery Mini Colors](https://labs.abeautifulsite.net/jquery-minicolors/) - Color picker
* [jQuery](http://jquery.com/) - Because the first two libraries abve require it
* [saveSvgAsPng](https://github.com/exupero/saveSvgAsPng) - Saves our SVG as a PNG
* [FileSaver.js](https://github.com/eligrey/FileSaver.js/) - Easy way to save files
* [SweetAlert2](https://sweetalert2.github.io/) - Stylish alert dialog
* [TinyColor](https://github.com/bgrins/TinyColor) - Converts color codes
* [Moveit](https://github.com/Raminsiach/Moveit) - SVG Path Animator

Design
-------------
* Currently all the characters were designed by [Michael Schwartz](http://michaelsboost.github.io/) using [Gravit Designer](https://designer.io/).

To-Do
-------------

- fix export to png on Android
- add more content to categories and assets

Development
-------------

Want to contribute? Great!  

I posted a video on YouTube showing you exactly how you can add a feature design (like front-hair, facial hair, eyes, body-types, etc:) to the application.

You can watch that video here - https://www.youtube.com/watch?v=8zpcx4210IE

I recommend designing the character in Gravit Designer because the code it outputs is cleaner and easier to manage than any other vector editor I've used. 

**NOTE**: You will not be able to change the fill color if fill is specified as a style attribute, but only as a fill html attribute.

<hr>

The Flat Design Character Maker is no longer an active project however you can submit a pull request or simply share the project :)

Of course the app is free and open source, so you can always fork the project and have fun :)

If the Flat Design Character Maker was at all helpful for you. You can show your appreciation by [Donating via SquareCash](https://cash.me/$michaelsboost) and/or [PayPal](https://www.paypal.me/mikethedj4)
